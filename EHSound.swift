//
//  Sound.swift
//  Prototope
//
//  Created by Andy Matuschak on 11/19/14.
//  Copyright (c) 2014 Khan Academy. All rights reserved.
//

import AVFoundation
import Foundation

/** Provides a simple way to play sound files. Supports .aif, .aiff, .wav, and .caf files. */
public struct EHSound: CustomStringConvertible {
    
    private let player: AVAudioPlayer
    private let name: String!
    private static var playingAVAudioPlayers = Set<AVAudioPlayer>()
    private static var playingAVAudioPlayerDelegates = Set<EHSound.AVAudioPlayerDelegate>()
    
    /** Creates a sound from a filename. No need to include the file extension: Prototope will
     try all the valid extensions. */
    public init?(name: String, type:String) throws {
        
        guard let patch = NSBundle.mainBundle().pathForResource(name, ofType: type) else {
            throw EHError.PathForResource("Can't find the patch")
        }
        
        guard let data = NSData(contentsOfFile: patch) else {
            throw EHError.NSData(whereIs: "EasySound",funcIs: "init?",errorIs:"Can't find data by the patch")
        }
        
        do {
            self.player = try AVAudioPlayer(data: data)
            self.player.prepareToPlay()
            self.name = name
        } catch {
            throw EHError.Error(whereIs: "EasySound",funcIs: "init?",errorIs: "\(error)")
        }
        
        
        
    }
    
    public init?(url:NSURL) throws {
        
        guard let data = NSData(contentsOfURL: url) else {
            throw EHError.NSData(whereIs: "EasySound",funcIs: "init?",errorIs:"Can't find data by the patch")
        }
        
        do {
            self.player = try AVAudioPlayer(data: data)
            self.player.prepareToPlay()
            self.name = ""
        } catch {
            throw EHError.Error(whereIs: "EasySound",funcIs: "init?",errorIs: "\(error)")
        }
    }

    public var description: String {
        return self.name
    }
    
    /// From 0.0 to 1.0
    public var volume: Double {
        get {
            return Double(player.volume)
        }
        set {
            player.volume = Float(newValue)
        }
    }
    
    public func play() {
        player.currentTime = 0
        if player.delegate == nil {
            let delegate = AVAudioPlayerDelegate()
            player.delegate = delegate
            EHSound.playingAVAudioPlayerDelegates.insert(delegate)
        }
        EHSound.playingAVAudioPlayers.insert(player)
        player.play()
    }
    
    public mutating func stop() {
        player.stop()
        if let delegate = (player.delegate as? EHSound.AVAudioPlayerDelegate) {
            EHSound.playingAVAudioPlayerDelegates.remove(delegate)
            player.delegate = nil
        }
        EHSound.playingAVAudioPlayers.remove(player)
    }
    
    public static let supportedExtensions = ["caf", "aif", "aiff", "wav"]
    
    // Fancy scheme to keep playing AVAudioPlayers from deallocating while they're playing.
    @objc private class AVAudioPlayerDelegate: NSObject, AVFoundation.AVAudioPlayerDelegate {
        @objc func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
            player.delegate = nil
            playingAVAudioPlayers.remove(player)
            playingAVAudioPlayerDelegates.remove(self)
        }
        
        @objc func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer, error: NSError?) {
            player.delegate = nil
            playingAVAudioPlayers.remove(player)
            playingAVAudioPlayerDelegates.remove(self)
        }
    }
}

public extension EHSound {
    public enum EHError: ErrorType, CustomStringConvertible {
        case Error(whereIs:String,funcIs:String, errorIs:String)
        case Nil(String)
        case PathForResource(String)
        case NSData(whereIs:String,funcIs:String, errorIs:String)
        case NSURL(String)
        case JSON(String)
        case NSDictionary(String)
        
        public var description: String {
            switch self {
            case Error(let whereIs,let funcIs, let errorString) : return "[EasyHelper \(whereIs)][\(funcIs)] \(errorString)"
            case Nil(let ePrint) : return ePrint
            case PathForResource(let ePrint) : return ePrint
            case NSData(let whereIs,let funcIs, let errorString) : return "[EasyHelper \(whereIs)][\(funcIs)] \(errorString)"
            case NSURL(let ePrint) : return ePrint
            case JSON(let ePrint) : return ePrint
            case NSDictionary(let ePrint) : return ePrint
            }
        }
        
        func printError() { print("[EHSound] \(self.description)") }
        
    }
}
